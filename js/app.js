//use window.scrollY
var scrollpos = window.scrollY;
var menuBar = document.getElementById("menu-bar");
var g = document.getElementById("menu-btn");

function add_class_on_scroll() {
    menuBar.setAttribute('style', 'background-color: #f8f8f8');
    menuBar.classList.add("fade-in");
    g.setAttribute('style', 'fill: #222');
}

function remove_class_on_scroll() {
    menuBar.setAttribute('style', 'background-color: transparent;');
    menuBar.classList.remove("fade-in");
    g.setAttribute('style', 'fill: #eee');
}

window.addEventListener('scroll', function(){
    //Here you forgot to update the value
    scrollpos = window.scrollY;

    if(scrollpos > 550){
        add_class_on_scroll();
    }
    else {
        remove_class_on_scroll();
    }
    // console.log(g);
});
